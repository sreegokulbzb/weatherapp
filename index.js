const ThiruvananthapuramButton = document.getElementById('Thiruvananthapuram')
const KollamButton = document.getElementById('Kollam')
const PathanamthittaButton = document.getElementById('Pathanamthitta')
const AlappuzhaButton = document.getElementById('Alappuzha')
const KottayamButton = document.getElementById('Kottayam')
const IdukkiButton = document.getElementById('Idukki')
const ErnakulamButton = document.getElementById('Ernakulam')
const ThrissurButton = document.getElementById('Thrissur')
const PalakkadButton = document.getElementById('Palakkad')
const KozhikodeButton = document.getElementById('Kozhikode')
const WayanadButton = document.getElementById("Wayanad")
const KannurButton = document.getElementById('Kannur')
const KasaragodButton = document.getElementById('Kasaragod')
const KlButton = document.getElementById('Kl')
const cityselection = document.getElementById('citysection')

ThiruvananthapuramButton.addEventListener("click" ,() => {
    getTemperature(8.5241, 76.9366,"Thiruvananthapuram")

})
    KollamButton.addEventListener("click" ,() => {
        getTemperature(8.8932, 76.6141,"Kollam")
})
PathanamthittaButton.addEventListener("click" ,() => {
    getTemperature(8.8932, 76.6141,"Pathanamthitta")
})
AlappuzhaButton.addEventListener("click" ,() => {
    getTemperature(9.4981, 76.3388,"Alappuzha")
})
KottayamButton.addEventListener("click" ,() => {
    getTemperature(9.5916, 76.5222,"Kottayam")
})
IdukkiButton.addEventListener("click" ,() => {
    getTemperature(9.8584, 76.9528, "Idukki")
})
ErnakulamButton.addEventListener("click" ,() => {
    getTemperature(9.9816, 76.2999, "Ernakulam")
})
ThrissurButton.addEventListener("click" ,() => {
    getTemperature(10.5276, 76.2144, "Thrissur")
})
PalakkadButton.addEventListener("click" ,() => {
    getTemperature(10.7867, 76.6548, "Palakkad")
})
KlButton.addEventListener("click" ,() => {
    getTemperature(10.7867, 76.6548, "Malappuram")
})

KozhikodeButton.addEventListener("click" ,() => {
    getTemperature(11.2588, 75.7804, "Kozhikode")
})
WayanadButton.addEventListener("click" ,() => {
    getTemperature(11.6854, 76.1320, "Wayanad")
})
KannurButton.addEventListener("click" ,() => {
    getTemperature(11.8745, 75.3704, "Kannur")
})
KasaragodButton.addEventListener("click" ,() => {
    getTemperature(12.4996, 74.9869, "Kasaragod")
})

function getTemperature (latitude,longitude,city){
     fetch(`https://api.open-meteo.com/v1/forecast?latitude=${latitude}&longitude=${longitude}&current=temperature_2m,wind_speed_10m&hourly=temperature_2m,relative_humidity_2m,wind_speed_10m`)
     .then(response => response.json())
     .then(data =>{
        console.log(data)
        const temperature = data.current.temperature_2m;
        const windspeed  = data.current.wind_speed_10m;
        addcard(temperature, windspeed, city)
     })
     .catch(error =>console.log(error));
}

function addcard (temperature, windspeed, city){
    let image;
    if(city === "Thiruvananthapuram"){
        image = "https://dynamic-media-cdn.tripadvisor.com/media/photo-o/0b/5d/df/27/at-kovalam-beach.jpg?w=1200&h=-1&s=1"
    }
    else if(city === "Kollam"){
        image = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRCsHIf51-kV0YE1UnEzVdG8iAbjjbMt1zKMg&s"
    }
    else if(city === "Pathanamthitta"){
        image = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQWM7Aa4-3DTrdgxlVZqk31WdUDuD7AXjCnDA&s"
    }
    else if(city === "Alappuzha"){
        image = "https://www.oyorooms.com/blog/wp-content/uploads/2017/12/Alleppey-min.jpg"
    }
    else if(city === "Kottayam"){
        image = "https://assets-news.housing.com/news/wp-content/uploads/2022/06/28084025/KOTTAYAM5.png"
    }
    else if(city === "Idukki"){
        image = "https://lh5.googleusercontent.com/p/AF1QipPSHYDvZ0zeFdtdg5xZOHzdt6ASLX8I5ICIvE8d=s296-w296-h168-n-k-no-v1"
    }
    else if(city === "Ernakulam"){
        image = "https://upload.wikimedia.org/wikipedia/commons/9/95/Kochi_International_Marina%2C_Bolgatty_Island%2C_Kerala%2C_India.jpg"
    }
    else if(city === "Thrissur"){
        image = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQWJrW8wkH4jsVJY5XVdw1TDutTuJb_cSqq6g&s"
    }
    else if(city === "Palakkad"){
        image = "https://travelsetu.com/apps/uploads/new_destinations_photos/destination/2023/12/15/82dad96fa9e73adb0007320de793f191_1000x1000.jpg"
    }
    else if(city === "Kozhikode"){
        image = "https://www.fabhotels.com/blog/wp-content/uploads/2024/02/cc551697-kozhikode-beach.jpg"
    }
    else if(city === "Wayanad"){
        image = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ3jRa1zYXP8x6yOxPB6RmjukAh7gMh2qFJrg&s"
    }
    else if(city === "Kannur"){
        image = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSX-rcCSdTZn6esJlMg4dxUVxXLa80mtPGOqA&s"
    }
    else if(city === "Kasaragod"){
        image = "https://www.keralatourism.org/images/destination/mobile/aquatourism_in_kasaragod20131107114603_306_1.jpg"
    }
    else{
        image = "https://spb.kerala.gov.in/sites/default/files/styles/inner_banner_image_850x460/public/2020-09/1_2_12.jpg?itok=bmpeJ1gr"
    }
   
    cityselection.innerHTML = `
    <article id="place">
                <h3>${city}</h3>
                <img src="${image}" alt="">
                <div id="div">
                    <span>temperature : ${temperature} &deg;C</span>
                    <span>windspeed : ${windspeed} m/s</span>
                </div>
    </article>
    `
} 
 
 


 
 
